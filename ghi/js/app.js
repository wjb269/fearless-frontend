function createdCard(name, description, pictureUrl, start_date, end_date, location) {
    const startDate = new Date(start_date);
    const endDate = new Date(end_date);

    return `
    <div class="col">
        <div class="card h-100 shadow">
            <img src="${pictureUrl}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <small class="text-body-secondary">${String(startDate.getMonth() + 1)}/${String(startDate.getDay() + 24)}/${String(startDate.getFullYear())}
            - ${String(endDate.getMonth() + 1)}/${String(endDate.getDay() + 24)}/${String(endDate.getFullYear())}</small>
            </div>
        </div>
    </div>
    `;
}

function errorMessage() {
    return `
    <div class="alert alert-dark" role="alert">
        There is a problem with your request!
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    try {
        const response = await fetch(url);
        if (!response.ok) {
            const html = errorMessage();
            const column = document.querySelector(".row");
            column.innerHTML = html;
            console.log("There was a problem with your request!")
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const detail = await detailResponse.json();
                    const title = detail.conference.name;
                    const description = detail.conference.description;
                    const pictureUrl = detail.conference.location.picture_url;
                    const start_date = detail.conference.starts;
                    const end_date = detail.conference.ends;
                    const location = detail.conference.location.name;
                    const html = createdCard(title, description, pictureUrl, start_date, end_date, location);
                    const column = document.querySelector(".row");
                    column.innerHTML += html;
                }
            }
            // const conference = data.conferences[3];
            // const nameTag = document.querySelector(".card-title");
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // try{
            //     const detailResponse = await fetch(detailUrl);
            // if(!detailResponse.ok){
            //     console.log("There was a problem with the deatil URL!");
            // }else{
            //     const detail = await detailResponse.json();
            //     const descriptionTag = document.querySelector(".card-text");
            //     descriptionTag.innerHTML = detail["conference"]["description"];
            //     console.log(detail);

            //     const imageTag = document.querySelector(".card-img-top");
            //     imageTag.src = detail["conference"]["location"]["picture_url"];
            // }
            // }catch(e){
            //     console.log("There is an error in your detail!");
            // }
        }
    } catch (e) {
        const html = errorMessage();
        const column = document.querySelector(".row");
        column.innerHTML = html;
        console.log("There is a problem in your request!");
    }

});
